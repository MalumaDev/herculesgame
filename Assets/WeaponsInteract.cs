﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponsInteract : InteractionWithPlayer
{

    public string UI_Text = "Prova";
    public AudioClip WeaponSound;
    public Image UI_Weapon;
    public Sprite image;


    override
    public void Interact(GameObject caller)
    {
        //GetComponent<PickUpRotate>().coinRotation = new Vector3(0, 500, 0);
    }

    override
    public void OpenUI()
    {
        Time.timeScale = 0;
        Text text = (Text)GameObject.Find("MessagePanelText").GetComponent(typeof(Text));
        text.text = UI_Text.Replace("\\n", "\n"); ;
        GameObject.Find("EventSystem").GetComponent<AudioSource>().PlayOneShot(WeaponSound);
        GameObject.Find("MessageButtonPanel").GetComponent<Button>().enabled = true;
        GameObject.Find("MessagePanelCanvas").GetComponent<Canvas>().enabled = true;

        UI_Weapon.sprite = image;
    }
}
