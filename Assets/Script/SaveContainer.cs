﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveContainer
{
    public string Level;
    public int Points;

    public List<String> ObjectTaken;
    public Vector3 Position;
}
