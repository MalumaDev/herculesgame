﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clouds : MonoBehaviour {


    public List<GameObject> clouds;
    public int numberofClouds=100;
    public int seed=200;
    public float minDistance=10;

    private Vector3 m_Min, m_Max;
    private Collider m_Collider;

    // Use this for initialization
    void Start () {


        m_Collider = GetComponent<Collider>();

        //Fetch the minimum and maximum bounds of the Collider volume
        m_Min = m_Collider.bounds.min;
        m_Max = m_Collider.bounds.max;


        Random.InitState(seed);


        for (int i = 0; i < numberofClouds; i++)
        {
            Vector3 position = new Vector3(Random.Range(m_Min.x,m_Max.x), Random.Range(m_Min.y, m_Max.y), Random.Range(m_Min.z, m_Max.z));
            
            Instantiate(clouds[Random.Range(0, clouds.Count)], position, Quaternion.identity,transform).transform.localScale=(new Vector3(1,5,1))* Random.Range(0f, 1.5f);
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
