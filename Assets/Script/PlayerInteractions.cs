﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractions : MonoBehaviour {

    public float rangeOfView=400;
	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        Ray ray = new Ray(transform.position + new Vector3(0, 1, 0), transform.forward);
        RaycastHit hit;

        Debug.DrawRay(transform.position+new Vector3(0,1,0), transform.forward* rangeOfView, Color.red);

        if (Physics.Raycast(ray,out hit, rangeOfView))
        {
            var inter = hit.collider.GetComponent<InteractionWithPlayer>();

            if (inter != null)
            {
                inter.Interact(gameObject);
            }

        }
    }
}
