﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public GameObject player;       //Public variable to store a reference to the player game object
    public float distanceUp = 2;
    public float distanceAway = 8;
    public float offsetHeight = 8;
    public float smooth = 2;
    private Vector3 offset;         //Private variable to store the offset distance between the player and camera
    private Vector3 targetPosition;

    // Use this for initialization
    void Start()
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        offset = new Vector3(0, 0, 0);
        offset.y = offsetHeight;

        //transform.position = player.transform.position + offset;
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        //targetPosition = player.transform.position + player.transform.up * distanceUp - player.transform.forward * distanceAway;
        targetPosition = player.transform.position + player.transform.up * distanceUp - new Vector3(0,0,1) * distanceAway;


        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * smooth);

        offset.y = offsetHeight;

        transform.LookAt(player.transform.position + offset);
    }
}
