﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {


    public GameObject Menu;
    public GameObject Startbutton;
    public GameObject ContinueButton;
    public String StartScene;

    [Header("Advance")]
    static public String SaveName;

    void Start () {

        if (File.Exists(GetSaveFile()))
        {

            ContinueButton.GetComponent<Button>().onClick.AddListener(() => {
                    StartCoroutine(ExecuteAfterTimeFirstLevel(0.4f, StaticVariables.save.Level));
                }
            );
            LoadGame();
        }
        else
        {
            ContinueButton.GetComponent<Button>().interactable = false;
            ContinueButton.GetComponentInChildren<Text>().color= new Color(141f/255f, 141f / 255f, 141f / 255f);

        }

        Startbutton.GetComponent<Button>().onClick.AddListener(() => {
                StartCoroutine(ExecuteAfterTimeFirstLevel(0.4f, null));
            }
        );



    }


    IEnumerator ExecuteAfterTimeFirstLevel(float time, string name)
    {
        yield return new WaitForSeconds(time);

        // Code to execute after the delay
        if (name == null)
        {
            FirstLevel(StartScene);
        }
        else { 
            SelectLevel(name);
        }
    }

    private void FirstLevel(string name)
    {
        StaticVariables.save = new SaveContainer();
        StaticVariables.save.Level=name;
        StaticVariables.save.Points = 0;
        StaticVariables.save.ObjectTaken = new System.Collections.Generic.List<string>();
        ChangeScene(name);
        
    }


    private void SelectLevel(string name)
    {
        ChangeScene(name);

    }

    static public void ChangeScene(String name)
    {
        SceneManager.LoadScene(name, LoadSceneMode.Single);
    }


    public void LoadGame()
    {
        String json = File.ReadAllText(GetSaveFile());
        StaticVariables.save= JsonUtility.FromJson<SaveContainer>(json);

    }

    public static string GetSaveFile()
    {
        if (SaveName == null)
        {
            SaveName = "gamesave";
        }
        return Application.persistentDataPath + "/" + SaveName + ".save";
    }

    static public void SaveGame()
    {
        File.WriteAllText(GetSaveFile(), JsonUtility.ToJson(StaticVariables.save));
        #if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
        #endif
    }

    static public void SaveGame(Vector3 position)
    {
        StaticVariables.save.Position=position;
        SaveGame();
    }

    static public void Quit()
    {
        Application.Quit();
    }

}
