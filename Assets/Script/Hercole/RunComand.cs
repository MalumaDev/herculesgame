﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RunComand : MonoBehaviour {

    private Animator mAnimator;
    private Vector3 moveDirection = Vector3.zero;
    Quaternion rotation;
    private CharacterController controller;
    private AudioSource audioSource;

    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    public Transform Sea;
    public float CharacterHeight = 4;

    public AudioClip walkSound;
    public AudioClip walkWaterSound;
    public AudioClip swimmingSound;
    public AudioClip deathSound;
    public AudioClip grassSound;

    private bool running = false;
    private bool jump = false;
    private bool swimming = false;
    private bool inWater = false;

    // Use this for initialization
    void Start () {
        mAnimator = GetComponent<Animator>();
        controller = GetComponent<CharacterController>();
        // let the gameObject fall down
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        audioSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

        if (swimming)
        {
            return;
        }
        
        if (controller.isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes

            moveDirection = Camera.main.transform.TransformDirection(new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")));
            moveDirection.y = 0;
            //moveDirection = transform.TransformDirection(moveDirection);

            moveDirection = moveDirection * speed;

            if (!jump && Input.GetButtonDown("Jump"))
            {
                moveDirection.y = jumpSpeed;
                jump = true;
                running = false;
            }
            else
            {
                //jump = false;
                if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
                {
                    running = true;
                    transform.rotation = Quaternion.LookRotation(moveDirection);
                }
                else
                {
                    running = false;
                }
            }

        }
        if (inWater)
        {

            if (Sea != null)
            {
                if (transform.position.y <= Sea.position.y - CharacterHeight)
                {
                    swimming = true;
                    running = false;
                    jump = false;

                    StartCoroutine(Died());
                }
            }
        }

        // Apply gravity

        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
        // Move the controller
        controller.Move(moveDirection * Time.deltaTime);



        if (Time.timeScale == 1 && audioSource != null)
        {
            if (running && !audioSource.isPlaying)
            {
                if (!inWater)
                {
                    audioSource.PlayOneShot(walkSound);
                }
                else
                {
                    audioSource.PlayOneShot(walkWaterSound);
                }
            }
            else if (swimming && !audioSource.isPlaying)
            {
                audioSource.PlayOneShot(walkSound);
            }
            else if (!running  && audioSource.isPlaying)
            {
                audioSource.Stop();
            }
        }
        else
        {
            audioSource.Stop();
        }

        mAnimator.SetBool("jump", jump);      
        mAnimator.SetBool("running", running);
        mAnimator.SetBool("swimming", swimming);


    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sea"))
        {
            inWater = true;
            audioSource.Stop();
        }

        if (other.CompareTag("Bush"))
        {
            if (Time.timeScale == 1 && audioSource != null)
            {
                audioSource.PlayOneShot(grassSound);
            }
        }

    }


    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Sea"))
        {
            inWater = false;
            audioSource.Stop();
        }
    }


    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;

        if (body == null)
        {
            //if (jump && hit.collider.CompareTag("Terrain"))
            if (jump)
            {
                jump = false;
                inWater = false;
                running = true;
            }
            return;
        }

        if (body.CompareTag("PickUp"))
        {
            if (StaticVariables.save != null)
            {
                StaticVariables.save.ObjectTaken.Add(body.name);
                StaticVariables.save.Position = transform.position;
                MainMenu.SaveGame(transform.position);

            }
            body.GetComponent<InteractionWithPlayer>().OpenUI();
            body.gameObject.SetActive(false);
        }

    }



    public string DeathText = "Un misterioso maleficio circonda l'isola,\\n verrai riportato indietro nel tempo";

    IEnumerator Died()
    {
        yield return new WaitForSeconds(0.5f);
        Time.timeScale = 0;
        Text text = (Text)GameObject.Find("MessagePanelText").GetComponent(typeof(Text));
        text.text = DeathText.Replace("\\n", "\n"); ;
        GameObject.Find("EventSystem").GetComponent<AudioSource>().PlayOneShot(deathSound);
        GameObject.Find("MessageButtonPanel").GetComponent<Button>().enabled = true;
        GameObject.Find("MessagePanelCanvas").GetComponent<Canvas>().enabled = true;


        yield return new WaitForSeconds(0.5f);
        swimming = false;

        transform.position = StaticVariables.save.Position;
    }
}
