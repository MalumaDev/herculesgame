﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractionWithPlayer : MonoBehaviour
{

    public abstract void Interact(GameObject caller);

    public abstract void OpenUI();

}
