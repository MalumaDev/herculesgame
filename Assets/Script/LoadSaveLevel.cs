﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSaveLevel : MonoBehaviour {

    public Transform Character;
    public Vector3 initPosition;

	// Use this for initialization
	void Start () {
        if (StaticVariables.save != null)
        {
            if (StaticVariables.save.ObjectTaken.Count != 0) {
                foreach (var obj in StaticVariables.save.ObjectTaken)
                {
                    try
                    {
                        GameObject.Find(obj).SetActive(false);
                    }
                    catch
                    {

                    }

                }
            }
            if (StaticVariables.save.Position.x != 0 && StaticVariables.save.Position.y != 0)
            {
                Character.position = StaticVariables.save.Position;
            }
            else
            {
                //Character.position = initPosition;
            }

        }
    }

}
