﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpRotate : MonoBehaviour {

    public Vector3 coinRotation= new Vector3(0, 30, 0);
    private Vector3 coinRotationTmp;

    // Use this for initialization
    void Start () {
        coinRotationTmp = coinRotation;

    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(coinRotation * Time.deltaTime);
        coinRotation = coinRotationTmp;
    }
}
