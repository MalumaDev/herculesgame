﻿using System;
using UnityEngine;

public class Level1KeyEvent : MonoBehaviour {

    public GameObject hero;
    public String MenuScene;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {

            StaticVariables.save.Position = hero.transform.position;
            MainMenu.SaveGame(transform.position);
            MainMenu.ChangeScene(MenuScene);
        }
        if (Input.GetKeyDown(KeyCode.F12))
        {
            foreach(GameObject tmp in GameObject.FindGameObjectsWithTag("PickUp"))
            {
                StaticVariables.save.ObjectTaken.Add(tmp.name);
                tmp.SetActive(false);
            }
            
        }



    }
}
