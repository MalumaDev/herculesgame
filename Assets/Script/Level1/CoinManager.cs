﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CoinManager : MonoBehaviour {

    private UnityEngine.UI.Text text;

	// Use this for initialization
	void Start () {
        text = (UnityEngine.UI.Text)GameObject.Find("CoinsNumber").GetComponent(typeof(Text));


        //TODO: DA RIMUOVERE
        if (StaticVariables.save == null)
        {
            StaticVariables.save = new SaveContainer();
            StaticVariables.save.Points = 0;
            StaticVariables.save.ObjectTaken = new System.Collections.Generic.List<string>();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(StaticVariables.save!=null)
            text.text = "x"+ StaticVariables.save.ObjectTaken.Count;
    }
}
