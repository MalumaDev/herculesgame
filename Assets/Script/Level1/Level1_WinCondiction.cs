﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1_WinCondiction : MonoBehaviour
{

    public int winNumber;
    public GameObject hero;
    public AudioClip victory_sound;
    public string NextScene;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 1)
        {
            if (winNumber != 0 && StaticVariables.save.ObjectTaken.Count == winNumber)
            {
                winNumber = 0;
                StartCoroutine(win());

            }
        }

    }

    IEnumerator win()
    {
        hero.GetComponent<CharacterController>().enabled = false;
        hero.GetComponent<RunComand>().enabled = false;
        hero.GetComponent<Animator>().SetBool("win", true);
        GetComponent<AudioSource>().PlayOneShot(victory_sound);

        StaticVariables.save.ObjectTaken.Clear();
        StaticVariables.save.Position = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(5);

        if (NextScene.Length > 0)
        {
            MainMenu.ChangeScene(NextScene);
        }
    }
}
