﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinInteract : InteractionWithPlayer{

    public string UI_Text = "Prova";
    public AudioClip CoinSound;


    override
    public void Interact(GameObject caller)
    {
        GetComponent<PickUpRotate>().coinRotation=new Vector3(0,500,0);
    }

    override
    public void OpenUI()
    {
        Time.timeScale = 0;
        Text text=(Text)GameObject.Find("MessagePanelText").GetComponent(typeof(Text));
        text.text = UI_Text.Replace("\\n", "\n"); ;
        GameObject.Find("EventSystem").GetComponent<AudioSource>().PlayOneShot(CoinSound);
        GameObject.Find("MessageButtonPanel").GetComponent<Button>().enabled = true;
        GameObject.Find("MessagePanelCanvas").GetComponent<Canvas>().enabled = true;
    }
}
